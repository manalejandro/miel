/*
  tarro de miel v1.0.0
  - muestra rutas de ataque
  - si hace peticion a una ruta que no existe, se le devuelve un 200, se crea y se graba la actividad en el log con el cuerpo
*/

const http = require('http'),
  express = require('express'),
  morgan = require('morgan'),
  rfs = require('rotating-file-stream'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  app = express(),
  accessLogStream = rfs('access.log', {
    interval: '1d',
    path: __dirname + '/logs'
  })

morgan.token('body', (req, res) => {
  return JSON.stringify(req.body)
})

app.use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(methodOverride())
  .use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":body"', {
    stream: accessLogStream,
    compress: 'gzip'
  }))
  .use('*', (req, res) => {
    res.status(200).end('Have a nice day :-)')
  })

const server = http.createServer(app).listen(parseInt(process.argv[2]), () => {
  console.log('Miel listening on ' + server.address().address + ':' + server.address().port)
})
